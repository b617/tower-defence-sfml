#include "GameHUD.h"
#include "..\GlobalVariables.h"
#include "..\Images\Library.h"


GameHUD::GameHUD()
{
	GlobalVariables::activeObjects.insert(this);
	GlobalVariables::drawnObjects[GlobalVariables::HUD].insert(this);

	lives.setFont(Library::GetInstance().Font_Standard);
	money.setFont(Library::GetInstance().Font_Standard);
	money.setPosition(0, 30);

	//create buttons
	sf::Vector2f buttonPosition(600, 0);
	sf::Vector2f buttonSize(150, 30);
	for (size_t i = 0; i < GlobalVariables::availableTowerTypes.size(); i++)
	{
		TowerType* towertype = GlobalVariables::availableTowerTypes[i];
		buttonsTowers.push_back(new HUDElements::Button(buttonPosition, buttonSize, towertype->name));
		buttonsTowers[i]->onClick.listeners.insert(this);
		buttonPosition += sf::Vector2f(buttonSize.x, 0);
	}
}


GameHUD::~GameHUD()
{
	GlobalVariables::activeObjects.erase(this);
	GlobalVariables::drawnObjects[GlobalVariables::HUD].erase(this);
}

void GameHUD::Update()
{
	lives.setString("Lives: " + std::to_string(GlobalVariables::gameManager.livesLeft));
	money.setString("Money: " + std::to_string(GlobalVariables::gameManager.money));
}

void GameHUD::OnEvent(EventDispatcher * sender)
{
	for (size_t i = 0; i < buttonsTowers.size(); i++)
	{
		if (sender == &buttonsTowers[i]->onClick)
		{
			GlobalVariables::buildingManager.selectedTowerType = GlobalVariables::availableTowerTypes[i];
			break;
		}
	}
}

void GameHUD::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(lives, states);
	target.draw(money, states);
	for (auto button : buttonsTowers) target.draw(*button, states);
}
