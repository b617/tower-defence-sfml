#include "HUDElements.h"
#include "..\Images\Library.h"
#include <iostream>

HUDElements::Button::Button(sf::Vector2f position, sf::Vector2f size, sf::String string)
{
	rect.setSize(size);
	rect.setFillColor(sf::Color(160, 160, 160));
	rect.setOutlineColor(sf::Color(50, 50, 50));
	rect.setOutlineThickness(2);
	rect.setPosition(position);

	text.setFillColor(sf::Color::Black);
	text.setString(string);
	text.setFont(Library::GetInstance().Font_Standard);
	text.setCharacterSize(20);
	text.setOrigin(text.getGlobalBounds().width / 2, text.getGlobalBounds().height / 2);
	text.setPosition(position + (0.5f*size));

	clickableObjects.insert(this);
}

HUDElements::Button::~Button()
{
	clickableObjects.erase(this);
}

sf::FloatRect HUDElements::Button::GetBounds()
{
	return rect.getGlobalBounds();
}

void HUDElements::Button::OnClick()
{
	onClick.Invoke();
}

void HUDElements::Button::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(rect, states);
	target.draw(text, states);
}

