#pragma once
#include <SFML\Graphics.hpp>
#include "..\Systems\Updatable.h"
#include "..\Systems\Events.h"
#include "HUDElements.h"


class GameHUD : public sf::Drawable, public Updatable, public EventListener
{
public:
	GameHUD();
	~GameHUD();

	virtual void Update();
	virtual void OnEvent(EventDispatcher* sender);
protected:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
public:

	sf::Text lives;
	sf::Text money;
	std::vector<HUDElements::Button*> buttonsTowers;
};

