#pragma once
#include <SFML\Graphics.hpp>
#include "..\Systems\Clickable.h"
#include "..\Systems\Events.h"

namespace HUDElements
{
	class Button : public sf::Drawable, public Clickable
	{
	public:
		Button(sf::Vector2f position, sf::Vector2f size, sf::String string);
		~Button();

		virtual sf::FloatRect GetBounds();
		virtual void OnClick();
	protected:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	public:
		EventDispatcher onClick;
		sf::RectangleShape rect;
		sf::Text text;
	};

};

