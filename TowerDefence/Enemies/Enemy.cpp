#include "Enemy.h"
#include "..\GlobalVariables.h"
#include "..\Math\CustomMath.h"

const float Enemy::rotationSpeed = 5.f;


Enemy::Enemy(EnemyType* type, Path* path) :type(type), path(path)
{
	//set graphics
	setTexture(type->image);
	setOrigin(getGlobalBounds().width / 2, getGlobalBounds().height / 2);
	setColor(type->color);
	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].insert(this);
	
	//set gameplay-related stuff
	HP = type->maxHP;
	NextWaypoint();
	setRotation(Math::LookAtRotation(getPosition(), targetWaypoint));
	GlobalVariables::activeObjects.insert(this);
	GlobalVariables::allEnemies.insert(this);
}

Enemy::~Enemy()
{
}

void Enemy::DealDamage(float amount)
{
	HP -= amount;
	if (HP < 0)
	{
		GlobalVariables::gameManager.money += type->moneyForKilling;
		MarkForDeletion();
	}
}


void Enemy::Update()
{
	sf::Vector2f dir = targetWaypoint - getPosition();
	float maxDistanceThisFrame = GlobalVariables::deltaTime * type->movementSpeed;

	if (Math::Length(dir) <= maxDistanceThisFrame)
	{
		//we're at target node
		setPosition(targetWaypoint);
		NextWaypoint();
		return;
	}

	sf::Vector2f dirNormalized = Math::Normalize(dir);

	move(dirNormalized * maxDistanceThisFrame);
	SmoothRotateRowards(dirNormalized);
}

void Enemy::Delete()
{
	onDeath.Invoke();
	GlobalVariables::activeObjects.erase(this);
	GlobalVariables::allEnemies.erase(this);
	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].erase(this);
	delete this;
}

void Enemy::NextWaypoint()
{
	if (currentWaypointIndex < path->size())
	{
		targetWaypoint = path->at(currentWaypointIndex);
		currentWaypointIndex++;
	}
	else
	{
		//Whole path has been traversed.
		ReachedTarget();
	}
}

void Enemy::ReachedTarget()
{
	GlobalVariables::gameManager.LoseLife(type->banishmentCost);
	MarkForDeletion();	//commit suicide
}

void Enemy::SmoothRotateRowards(sf::Vector2f targetRotation)
{
	sf::Vector2f currentRotation = Math::AngleToDirectionVector(getRotation()*Math::toRadians);
	sf::Vector2f resultRotation = Math::Lerp(currentRotation, targetRotation, GlobalVariables::deltaTime*rotationSpeed);
	setRotation(Math::DirectionToAngle(resultRotation));
}
