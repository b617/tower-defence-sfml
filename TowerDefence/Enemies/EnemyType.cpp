#include "EnemyType.h"

EnemyType::EnemyType(const sf::Texture& image, const char* name) : image(image), name(name)
{
}


EnemyType::~EnemyType()
{
}

EnemyType * EnemyType::SetMovementSpeed(float newSpeed)
{
	movementSpeed = newSpeed;
	return this;
}
EnemyType * EnemyType::SetMaxHP(float newMaxHP)
{
	maxHP = newMaxHP;
	return this;
}
EnemyType * EnemyType::SetColorTint(sf::Color newColor)
{
	color = newColor;
	return this;
}
EnemyType * EnemyType::SetBanishmentCost(int newBanishmentCost)
{
	banishmentCost = newBanishmentCost;
	return this;
}

EnemyType * EnemyType::SetMoneyValueForKilling(int newMoneyForKilling)
{
	moneyForKilling = newMoneyForKilling;
	return this;
}
