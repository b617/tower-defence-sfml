#pragma once
#include <SFML\Graphics.hpp>

class EnemyType
{
public:
	EnemyType(const sf::Texture& image, const char* name);
	~EnemyType();

	EnemyType* SetMovementSpeed(float newSpeed);
	EnemyType* SetMaxHP(float newMaxHP);
	EnemyType* SetColorTint(sf::Color newColor);
	EnemyType* SetBanishmentCost(int newBanishmentCost);
	EnemyType* SetMoneyValueForKilling(int newMoneyForKilling);

	const char* name = "Generic Enemy";
	const sf::Texture& image;
	sf::Color color = sf::Color::White;
	float maxHP = 100.0f;
	float movementSpeed = 50.0f;
	int banishmentCost = 1;
	int moneyForKilling = 1;
};

