#pragma once
#include "EnemyType.h"
#include "..\Math\Pathing.h"
#include "..\Systems\Updatable.h"
#include "..\Systems\Deletable.h"
#include "..\Systems\Events.h"


class Enemy : public sf::Sprite, public Updatable, public Deletable
{
public:
	Enemy(EnemyType* type, Path* path);
	~Enemy();

	void DealDamage(float amount);

	virtual void Update();
	virtual void Delete();
private:
	void NextWaypoint();
	void ReachedTarget();
	void SmoothRotateRowards(sf::Vector2f targetRotation);

public:

	static const float rotationSpeed;

	EnemyType* type;
	float HP;
	sf::Vector2f targetWaypoint;
	size_t currentWaypointIndex = 0;
	Path* path;
	EventDispatcher onDeath;
};

