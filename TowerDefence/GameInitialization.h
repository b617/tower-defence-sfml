#pragma once
#include "GlobalVariables.h"
#include "Math\Pathing.h"

namespace GameInitialization
{

	void InitializeTowerTypes()
	{
		Library& lib = Library::GetInstance();
		GlobalVariables::availableTowerTypes.push_back((new TowerType(lib.T_Tower, "Long ranged"))->
			SetColorTint(sf::Color::Cyan)->SetRange(1000)->SetShootingCooldown(1)->SetCost(2)->SetMissileData(TowerType::MissileData(600, 20, 6)));
		GlobalVariables::availableTowerTypes.push_back((new TowerType(lib.T_Tower, "Fast firerate"))->
			SetColorTint(sf::Color::Blue)->SetShootingCooldown(0.3f)->SetCost(3)->SetMissileData(TowerType::MissileData(400, 3, 3)));
		GlobalVariables::availableTowerTypes.push_back((new TowerType(lib.T_Tower, "Heavy"))->
			SetColorTint(sf::Color::Red)->SetShootingCooldown(2.5f)->SetCost(5)->SetRange(300)->SetMissileData(TowerType::MissileData(300, 50)));
	}
	
	void InitializeWaves()
	{

	}

	Path* InitializeLevel()
	{
		return nullptr;
	}

	void InitializeEverything()
	{
		InitializeTowerTypes();
	}

};
