#pragma once
#include <SFML\Graphics.hpp>
#include "..\Systems\Clickable.h"
#include "..\Systems\Deletable.h"

class TowerPlace : public sf::RectangleShape, public Clickable, public Deletable
{
public:
	TowerPlace(sf::Vector2f position);
	~TowerPlace();

	virtual sf::FloatRect GetBounds();
	virtual void OnClick();
	virtual void Delete();
};

