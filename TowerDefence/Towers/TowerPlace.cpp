#include "TowerPlace.h"
#include "..\GlobalVariables.h"



TowerPlace::TowerPlace(sf::Vector2f position) :RectangleShape(sf::Vector2f(50.f, 50.f))
{
	setFillColor(sf::Color::Yellow);
	setOutlineColor(sf::Color::Color(120, 60, 5));
	setOrigin(getGlobalBounds().width / 2, getGlobalBounds().height / 2);
	setPosition(position);

	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].insert(this);
	clickableObjects.insert(this);
}


TowerPlace::~TowerPlace()
{
}

sf::FloatRect TowerPlace::GetBounds()
{
	return getGlobalBounds();
}

void TowerPlace::OnClick()
{
	if (GlobalVariables::buildingManager.CreateTower(getPosition()))
	{
		MarkForDeletion();
	}
}

void TowerPlace::Delete()
{
	clickableObjects.erase(this);
	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].erase(this);
	delete this;
}
