#pragma once
#include <SFML\Graphics.hpp>
#include <vector>
#include "Missile.h"
#include "..\Enemies\Enemy.h"

class TowerType
{
public:
	struct MissileData
	{
		float speed = 400.f;
		float radius = 10.f;
		float damage = 20.0f;
		sf::Color color = sf::Color::Red;

		MissileData(float speed = 400.0f, float damage = 20.f, float radius = 10.f, sf::Color color = sf::Color::Red)
		{
			this->speed = speed;
			this->damage = damage;
			this->radius = radius;
			this->color = color;
		}
	};

	TowerType(sf::Texture& image, const char* name);
	~TowerType();

	TowerType* SetColorTint(sf::Color newColor);
	TowerType* SetRange(float newRange);
	TowerType* SetMissileData(MissileData newData);
	TowerType* SetShootingCooldown(float newCooldown);
	TowerType* SetCost(int newCost);

	void SpawnMissile(sf::Vector2f position, Enemy* target);

	sf::Texture& image;
	sf::Color color = sf::Color::White;
	const char* name;
	int cost = 3;
	float range = 500.f;
	float shootingCooldown = 0.5f;
	MissileData missileData;
	std::vector<TowerType*> possibleUpgrades;
};

