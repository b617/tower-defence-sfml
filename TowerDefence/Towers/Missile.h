#pragma once
#include <SFML\Graphics.hpp>
#include "..\Systems\Updatable.h"
#include "..\Systems\Deletable.h"
#include "..\Enemies\Enemy.h"
#include "..\Systems\Events.h"

class Missile : public sf::CircleShape, public Deletable, public Updatable, public EventListener
{
public:
	Missile(Enemy* target, float radius);
	~Missile();

	virtual void Update();
	virtual void Delete();
	virtual void OnEvent(EventDispatcher* sender);
private:
	void HitEnemy();
public:
	Enemy* target;
	float speed;
	float damage;
};

