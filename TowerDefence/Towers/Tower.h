#pragma once
#include <SFML\Graphics.hpp>
#include "TowerType.h"
#include "..\Systems\Deletable.h"
#include "..\Systems\Updatable.h"
#include "..\Enemies\Enemy.h"

class Tower : public sf::Sprite, public Updatable, public Deletable
{
public:
	Tower(TowerType* type, sf::Vector2f position = sf::Vector2f());
	~Tower();

	virtual void Update();
	virtual void Delete();
private:
	void Shoot(Enemy* target);
public:
	TowerType* type;
	float shootingCooldown = 0.0f;
};

