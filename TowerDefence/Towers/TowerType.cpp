#include "TowerType.h"

TowerType::TowerType(sf::Texture& image, const char* name) :image(image), name(name)
{
}


TowerType::~TowerType()
{
}

TowerType * TowerType::SetColorTint(sf::Color newColor)
{
	color = newColor;
	return this;
}

TowerType * TowerType::SetRange(float newRange)
{
	range = newRange;
	return this;
}

TowerType * TowerType::SetMissileData(MissileData newData)
{
	missileData = newData;
	return this;
}

TowerType * TowerType::SetShootingCooldown(float newCooldown)
{
	shootingCooldown = newCooldown;
	return this;
}

TowerType * TowerType::SetCost(int newCost)
{
	cost = newCost;
	return this;
}

void TowerType::SpawnMissile(sf::Vector2f position, Enemy * target)
{
	Missile* missile = new Missile(target, missileData.radius);
	missile->speed = missileData.speed;
	missile->setFillColor(missileData.color);
	missile->damage = missileData.damage;
	missile->setPosition(position);
}
