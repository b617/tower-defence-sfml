#include "Missile.h"
#include "..\Math\CustomMath.h"
#include "..\GlobalVariables.h"


Missile::Missile(Enemy* target, float radius) :CircleShape(radius), target(target)
{
	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].insert(this);
	GlobalVariables::activeObjects.insert(this);
	target->onDeath.listeners.insert(this);
}


Missile::~Missile()
{
}

void Missile::Update()
{
	sf::Vector2f dir = target->getPosition() - getPosition();
	float maxDistanceThisFrame = GlobalVariables::deltaTime * speed;

	if (Math::Length(dir) <= maxDistanceThisFrame)
	{
		HitEnemy();
	}

	sf::Vector2f dirNormalized = Math::Normalize(dir);
	move(dirNormalized * maxDistanceThisFrame);
}

void Missile::Delete()
{
	GlobalVariables::activeObjects.erase(this);
	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].erase(this);
	delete this;
}

void Missile::OnEvent(EventDispatcher * sender)
{
	//target enemy has been destroyed
	MarkForDeletion();
}

void Missile::HitEnemy()
{
	target->DealDamage(damage);
	target->onDeath.listeners.erase(this);
	MarkForDeletion();
}
