#include "Tower.h"
#include "..\GlobalVariables.h"
#include "..\Math\CustomMath.h"


Tower::Tower(TowerType* type, sf::Vector2f position) :type(type)
{
	//set graphics
	setTexture(type->image);
	setColor(type->color);
	setOrigin(getGlobalBounds().width / 2, getGlobalBounds().height / 2);
	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].insert(this);
	
	//set gameplay-related stuff
	setPosition(position);
	GlobalVariables::activeObjects.insert(this);
}


Tower::~Tower()
{
}

void Tower::Update()
{
	shootingCooldown -= GlobalVariables::deltaTime;

	Enemy* nearestEnemy = nullptr;
	float distanceToNearest = INFINITY;

	for (auto enemy : GlobalVariables::allEnemies)
	{
		float distance = Math::Distance(getPosition(), enemy->getPosition());
		if (distance < distanceToNearest)
		{
			distanceToNearest = distance;
			nearestEnemy = enemy;
		}
	}

	if (nullptr == nearestEnemy)
	{
		//printf("Turret %u does not see any enemies.\n", (size_t)(this));
		return;
	}

	setRotation(Math::LookAtRotation(getPosition(), nearestEnemy->getPosition()));

	if (distanceToNearest > type->range) return;

	if (shootingCooldown <= 0.0f)
	{
		shootingCooldown = type->shootingCooldown;
		Shoot(nearestEnemy);
		
	}
}

void Tower::Delete()
{
	GlobalVariables::activeObjects.erase(this);
	GlobalVariables::drawnObjects[GlobalVariables::FOREGROUND].erase(this);
	delete this;
}

void Tower::Shoot(Enemy* target)
{
	type->SpawnMissile(getPosition(), target);
}
