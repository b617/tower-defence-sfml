#pragma region Region_Includes
//Standard libraries
#include <stdlib.h>
#include <iostream>
//Additional dependencies
#include <SFML\Graphics.hpp>
//Classes
#include "Math\Pathing.h"
#include "Enemies\Enemy.h"
#include "Images\Library.h"
#include "GlobalVariables.h"
#include "Towers\Tower.h"
#include "HUD\GameHUD.h"
#include "Systems\Clickable.h"
#include "Towers\TowerPlace.h"
#include "GameInitialization.h"
#pragma endregion


int main()
{
	//Window settings
	unsigned int window_width = 1024;
	unsigned int window_height = 800;
	sf::RenderWindow window(sf::VideoMode(window_width, window_height), "My Tower Defence", sf::Style::Default);
	sf::Event event;

	//Some game variables
	GameInitialization::InitializeEverything();
	Library& lib = Library::GetInstance();
	Path* path = Pathing::CreatePath(new sf::Vector2f[7]{ 
		sf::Vector2f(150,350),sf::Vector2f(500,400),sf::Vector2f(750,200),sf::Vector2f(525,100),sf::Vector2f(350,150),sf::Vector2f(400,250),sf::Vector2f(800,300)
	}, 7);


	GameHUD HUD;
	new TowerPlace(sf::Vector2f(700, 100));
	new TowerPlace(sf::Vector2f(1000, 150));
	EnemyType* enemyType0 = (new EnemyType(lib.T_Enemy, "Enemy Type 0"))->SetMovementSpeed(150)->SetColorTint(sf::Color::Blue);
	Enemy* enemy = new Enemy(enemyType0, path);
	TowerType* towerType0 = (new TowerType(lib.T_Tower, "Test tower"))->SetColorTint(sf::Color::Green);
	//GlobalVariables::buildingManager.selectedTowerType = towerType0;
	
	
	//Engine variables
	sf::Clock clock;

	// Main loop
	while (window.isOpen())
	{
		//poll events
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) window.close();
			if (event.type == sf::Event::MouseButtonPressed)
			{
				sf::Vector2f mousePosition = (sf::Vector2f)sf::Mouse::getPosition(window);
				Clickable::ResolveClick(mousePosition);
			}
		}
		//manage time
		GlobalVariables::deltaTime = clock.restart().asSeconds();
		//update objects
		while (GlobalVariables::markedForDeletion.size() > 0)
		{
			GlobalVariables::markedForDeletion[0]->Delete();
			GlobalVariables::markedForDeletion.erase(GlobalVariables::markedForDeletion.begin());
		}
		for (auto obj : GlobalVariables::activeObjects) obj->Update();

		//draw objects
		window.clear();
		for (size_t i = 0; i < GlobalVariables::DRAW_LAYERS::COUNT; i++)
		{
			for (auto obj : GlobalVariables::drawnObjects[i]) window.draw(*obj);
		}
		window.display();
	}
	return EXIT_SUCCESS;
}