#include "GlobalVariables.h"



GlobalVariables::GlobalVariables()
{
}


GlobalVariables::~GlobalVariables()
{
}


GameManager GlobalVariables::gameManager(10);
BuildingManager GlobalVariables::buildingManager;
float GlobalVariables::deltaTime = 0.0f;
std::set<Updatable*> GlobalVariables::activeObjects;
std::set<sf::Drawable*> GlobalVariables::drawnObjects[GlobalVariables::DRAW_LAYERS::COUNT];
std::vector<Deletable*> GlobalVariables::markedForDeletion;
std::set<Enemy*> GlobalVariables::allEnemies;
std::vector<TowerType*> GlobalVariables::availableTowerTypes;
