#pragma once
#include <set>
#include <vector>
#include "Systems\Updatable.h"
#include "Systems\Deletable.h"
#include <SFML\Graphics\Drawable.hpp>
#include "Enemies\Enemy.h"
#include "Game\GameManager.h"
#include "Game\BuildingManager.h"
#include "Towers\TowerType.h"

class GlobalVariables
{
private:
	GlobalVariables();
	~GlobalVariables();
public:
	enum DRAW_LAYERS
	{
		BACKGROUND, FOREGROUND, HUD, COUNT
	};

	static float deltaTime;
	static GameManager gameManager;
	static BuildingManager buildingManager;
	static std::set<Updatable*> activeObjects;
	static std::set<sf::Drawable*> drawnObjects[COUNT];
	static std::vector<Deletable*> markedForDeletion;
	static std::set<Enemy*> allEnemies;
	static std::vector<TowerType*> availableTowerTypes;
};
