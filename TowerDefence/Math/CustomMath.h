#pragma once
#include <SFML\System\Vector2.hpp>

namespace Math
{
	static const float PI = 3.14159265358979323846f;
	static const float toRadians = PI/180;
	static const float toDegrees = 180/PI;

	template<class T>
	T Lerp(T val1, T val2, float alpha)
	{
		return (val1 * (1.0f - alpha)) + (val2 * alpha);
	}

	template<class T>
	T Length(sf::Vector2<T> vec)
	{
		return sqrt((vec.x * vec.x) + (vec.y * vec.y));
	}
	
	template<class T>
	sf::Vector2<T> Normalize(sf::Vector2<T> vec)
	{
		T length = Length(vec);
		if (length == 0) return sf::Vector2<T>(0, 0);
		return sf::Vector2<T>(vec.x / length, vec.y / length);
	}

	template<class T>
	T Distance(sf::Vector2<T> from, sf::Vector2<T> to)
	{
		return Length(to - from);
	}

	float DirectionToAngle(sf::Vector2f vec);

	sf::Vector2f AngleToDirectionVector(float angle);

	float LookAtRotation(sf::Vector2f source, sf::Vector2f target);
}
