#include "Pathing.h"
#include "CustomMath.h"

/* PATH */

Pathing::BasicPath::BasicPath(Path points) :points(points)
{
	//Calculate path length at different points for easy searching
	float length = 0.0f;
	pathLengthsAtPoints.push_back(0.0f);
	for (size_t i = 1; i < points.size(); i++)
	{
		length += Math::Length(points[i] - points[i - 1]);
		pathLengthsAtPoints.push_back(length);
	}
}


Pathing::BasicPath::~BasicPath()
{
}

sf::Vector2f Pathing::BasicPath::GetPointAtLength(float length)
{
	return sf::Vector2f();
}

float Pathing::BasicPath::GetLength()
{
	return 0.0f;
}

float Pathing::BasicPath::GetRotationAtLength()
{
	return 0.0f;
}



/* CUBIC SPLINE */

Pathing::CubicSpline::CubicSpline(std::vector<sf::Vector2f> points)
{
}

Pathing::CubicSpline::~CubicSpline()
{
}

Path * Pathing::CreatePath(sf::Vector2f points[], size_t pointCount)
{
	return new Path(points, points + pointCount);
}
