#pragma once
#include <vector>
#include <SFML\System\Vector2.hpp>

typedef std::vector<sf::Vector2f> Path;

namespace Pathing
{
	Path* CreatePath(sf::Vector2f points[], size_t pointCount);


	class BasicPath
	{
	public:
		BasicPath(Path points);
		~BasicPath();	
	protected:
			BasicPath() {}		//for direct inheritance
	public:

		virtual sf::Vector2f GetPointAtLength(float length);
		virtual float GetLength();
		virtual float GetRotationAtLength();

		Path points;


	protected:
		float pathLength;
	private:
		std::vector<float> pathLengthsAtPoints;
	};


	class CubicSpline : public BasicPath
	{
	public:
		CubicSpline(std::vector<sf::Vector2f> points);
		~CubicSpline();

	};

}
