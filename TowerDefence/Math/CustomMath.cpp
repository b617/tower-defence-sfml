#include "CustomMath.h"
#include <math.h>

float Math::DirectionToAngle(sf::Vector2f vec)
{
	return atan2f(vec.y, vec.x)*180.0f / PI;
}

sf::Vector2f Math::AngleToDirectionVector(float angle)
{
	return sf::Vector2f(cosf(angle),sinf(angle));
}

float Math::LookAtRotation(sf::Vector2f source, sf::Vector2f target)
{
	return DirectionToAngle(Normalize(target - source));
}
