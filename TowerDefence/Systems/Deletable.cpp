#include "Deletable.h"
#include "..\GlobalVariables.h"

void Deletable::MarkForDeletion()
{
	if (markedForDeletion) return;
	markedForDeletion = true;
	GlobalVariables::markedForDeletion.push_back(this);
}

bool Deletable::isValid()
{
	return !markedForDeletion;
}
