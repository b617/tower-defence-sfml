#pragma once
class Deletable
{
public:
	virtual void Delete() = 0;
	void MarkForDeletion();
	bool isValid();
private:
	bool markedForDeletion = false;
};

