#pragma once
#include <set>

class EventListener;

class EventDispatcher
{
public:
	EventDispatcher();
	~EventDispatcher();

	void Invoke();

	std::set<EventListener*> listeners;
};


class EventListener
{
public:
	virtual void OnEvent(EventDispatcher* sender) = 0;
};
