#include "Events.h"

EventDispatcher::EventDispatcher()
{

}

EventDispatcher::~EventDispatcher()
{
	listeners.clear();
}

void EventDispatcher::Invoke()
{
	for (auto i : listeners) i->OnEvent(this);
}
