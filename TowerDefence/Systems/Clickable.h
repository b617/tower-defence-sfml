#pragma once
#include <SFML\Graphics.hpp>
#include <set>

class Clickable
{
public:
	virtual sf::FloatRect GetBounds() = 0;
	virtual void OnClick() = 0;

	static bool ResolveClick(sf::Vector2f position);

	static std::set<Clickable*> clickableObjects;
};

