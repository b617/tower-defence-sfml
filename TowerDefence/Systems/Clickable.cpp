#include "Clickable.h"

std::set<Clickable*> Clickable::clickableObjects;

bool Clickable::ResolveClick(sf::Vector2f position)
{
	for (Clickable* c : clickableObjects)
	{
		if (c->GetBounds().contains(position))
		{
			c->OnClick();
			return true;
		}
	}

	return false;
}
