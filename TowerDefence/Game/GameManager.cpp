#include "GameManager.h"

GameManager::GameManager(int money, int lives) :livesLeft(lives), money(money)
{

}


GameManager::~GameManager()
{
}

void GameManager::LoseLife(int amount)
{
	livesLeft -= amount;
	if (livesLeft <= 0)
	{
		GameOver();
	}
}

void GameManager::GameOver()
{
	printf("GAME OVER\n");
	onGameOver.Invoke();
}
