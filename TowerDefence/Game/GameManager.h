#pragma once
#include "..\stdafx.h"
#include "..\Systems\Events.h"

class GameManager
{
public:
	GameManager(int money, int lives = 20);
	~GameManager();

	void LoseLife(int amount = 1);
	void GameOver();

	int livesLeft;
	int money;
	int score = 0;

	EventDispatcher onGameOver;
};

