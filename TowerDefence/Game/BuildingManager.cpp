#include "BuildingManager.h"
#include "..\GlobalVariables.h"


BuildingManager::BuildingManager()
{
}


BuildingManager::~BuildingManager()
{
}

bool BuildingManager::CreateTower(sf::Vector2f position)
{
	if (nullptr == selectedTowerType) return false;
	if (GlobalVariables::gameManager.money < selectedTowerType->cost) return false;

	GlobalVariables::gameManager.money -= selectedTowerType->cost;
	new Tower(selectedTowerType, position);
	return true;
}
