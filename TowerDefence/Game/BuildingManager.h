#pragma once
#include "../Towers/TowerType.h"
#include "../Towers/Tower.h"

class BuildingManager
{
public:
	BuildingManager();
	~BuildingManager();

	bool CreateTower(sf::Vector2f position);

	TowerType* selectedTowerType = nullptr;
};

