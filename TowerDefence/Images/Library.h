#pragma once
#include <SFML\Graphics.hpp>

class Library
{
private:
	Library();
	void LoadTexture(sf::Texture& target, const std::string & filepath);
	void LoadFont(sf::Font& target, const std::string & filepath);
public:
	~Library();

	static Library& GetInstance();

	sf::Texture T_Enemy;
	sf::Texture T_Tower;
	sf::Texture T_TowerPlace;

	sf::Font Font_Standard;
};

