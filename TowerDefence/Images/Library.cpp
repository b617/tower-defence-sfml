#include "Library.h"
#include <iostream>


Library::Library()
{
	LoadTexture(T_Enemy, "Images/Enemy.png");
	LoadTexture(T_Tower, "Images/Tower.png");
	LoadTexture(T_TowerPlace, "Images/TowerPlace.png");

	LoadFont(Font_Standard, "Images/Arialn.TTF");
}

void Library::LoadTexture(sf::Texture & target, const std::string & filepath)
{
	if (!target.loadFromFile(filepath))
	{
		std::cerr << "Texture couldn't be loaded. Filepath:\n" << filepath << std::endl;
	}
}

void Library::LoadFont(sf::Font & target, const std::string & filepath)
{
	if (!target.loadFromFile(filepath))
	{
		std::cerr << "Found couldn't be loaded. Filepath:\n" << filepath << std::endl;
	}
}


Library::~Library()
{

}

Library & Library::GetInstance()
{
	static Library library;
	return library;
}
